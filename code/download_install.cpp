//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          DOWNLOADINSTALL
//          Copyright 2022 -2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include <QProcess>
#include <QDir>
#include <QMessageBox>
#include <QStandardPaths>
#include "download_install.h"
#include "./ui_download_install.h"

DownloadInstall::DownloadInstall(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DownloadInstall)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
#endif
    ui->setupUi(this);
    this->setFixedSize(386, 200);
    QFont systemfont = QApplication::font();
    // qDebug() << "systemfont download_install " << systemfont;
    this->setFont(systemfont);
}

void DownloadInstall::setValues(const QString *installer_path, const QString *installer_filename, const QString *display_name, const QString *version)
{
    this->setWindowTitle(tr("Download ") + *display_name);
    QUrl url(*installer_path + *installer_filename);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
    reply->ignoreSslErrors();
// Cancel
    connect(ui->pbCancel, &QPushButton::clicked, [reply, this]() {
        reply->abort();
        close();
    });

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    connect(reply, &QNetworkReply::errorOccurred, [this, reply, version, display_name](QNetworkReply::NetworkError error) {
        QMessageBox *msgBox = new QMessageBox(this);
        msgBox->setIcon(QMessageBox::Critical);
        msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox->setWindowTitle(*display_name + " " + *version);
        msgBox->setText(networkErrorMessages(error));
        reply->disconnect();
        msgBox->exec();
        return;
    });

#endif
#if (QT_VERSION < QT_VERSION_CHECK(5, 15, 0))
    connect(reply, static_cast<void(QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error), [ = ](QNetworkReply::NetworkError error) {
        QMessageBox *msgBox = new QMessageBox(this);
        msgBox->setIcon(QMessageBox::Critical);
        msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox->setWindowTitle(*display_name + " " + *version);
        msgBox->setText(networkErrorMessages(error));
        reply->disconnect();
        msgBox->exec();
        return;
    });

#endif
    connect(reply, &QIODevice::readyRead, [this]() {
        ui->progressBar->setValue(ui->progressBar->value() + 1);

        if(ui->progressBar->value() >= 200) {
            ui->progressBar->setValue(0);
        }
    });

    QObject::connect(
        reply, &QNetworkReply::finished,
    [reply, installer_filename, this]() {
        ui->lblDownloading->setText(tr("The download is complete.<br>The current version will be uninstalled before the new one is installed."));
        QByteArray bytes = reply->readAll(); // bytes
        const QString tmppath = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
        QDir::setCurrent(tmppath);
        QFile file(tmppath + "/" + *installer_filename);
        file.open(QIODevice::ReadWrite);
        file.write(bytes, bytes.size());
        file.close();
        ui->progressBar->setValue(200);
        ui->pbInstall->setEnabled(true);
    });

// INSTALL
    connect(ui->pbInstall, &QPushButton::clicked, [this, installer_filename]() {
        close();
        const QString tmppath = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
        QDir::setCurrent(tmppath);
        QProcess::startDetached(tmppath + "/" + *installer_filename, QStringList() << "");
    });
}



DownloadInstall::~DownloadInstall()
{
//    delete ui;
}
