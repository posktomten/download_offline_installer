<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>DownloadInstall</name>
    <message>
        <source>Dialog</source>
        <translation>Finestra</translation>
    </message>
    <message>
        <source>Downloading...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>Installa</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Download </source>
        <translation>Download </translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>The download is complete.&lt;br&gt;The current version will be uninstalled before the new one is installed.</source>
        <translation>Il download è stata completato.&lt;br&gt;Prima verrà disinstallata la versione attuale quindi installata la nuova versione.</translation>
    </message>
    <message>
        <source>No error.</source>
        <translation>Nessun errore.</translation>
    </message>
    <message>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Il server remoto ha chiuso prematuramente la connessione, prima che l&apos;intera risposta fosse ricevuta ed elaborata.</translation>
    </message>
    <message>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Non è stato trovato il nome host remoto (nome host non valido).</translation>
    </message>
    <message>
        <source>The connection to the remote server timed out.</source>
        <translation>Timeout della connessione verso il server remoto.</translation>
    </message>
    <message>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>L&apos;operazione è stata annullata tramite chiamate a abort() o close() prima che fosse terminata.</translation>
    </message>
    <message>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>L&apos;handshake SSL/TLS non è riuscito e non è stato possibile stabilire il canale crittografato.&lt;br&gt;Dovrebbe essere stato emesso il segnale sslErrors().</translation>
    </message>
    <message>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>La connessione è stata interrotta a causa della disconnessione dalla rete, tuttavia il sistema ha avviato il roaming verso un altro punto di accesso.&lt;br&gt;La richiesta deve essere ripresentata e sarà elaborata non appena la connessione sarà ristabilita.</translation>
    </message>
    <message>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>La connessione è stata interrotta a causa della disconnessione dalla rete o del mancato avvio della rete.</translation>
    </message>
    <message>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>La richiesta in background non è attualmente consentita a causa della politica della piattaforma.</translation>
    </message>
    <message>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>È stato raggiunto il limite massimo durante i reindirizzamenti.&lt;br&gt;Per impostazione predefinita, il limite è impostato su 50 o impostato da QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Durante i reindirizzamenti, l&apos;API di accesso alla rete ha rilevato un reindirizzamento da un protocollo crittografato (https) a uno non crittografato (http).</translation>
    </message>
    <message>
        <source>An unknown network-related error was detected.</source>
        <translation>È stato rilevato un errore di rete sconosciuto.</translation>
    </message>
    <message>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>La connessione al server proxy è stata rifiutata (il server proxy non accetta richieste).</translation>
    </message>
    <message>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Il server proxy ha chiuso prematuramente la connessione, prima che l&apos;intera risposta fosse ricevuta ed elaborata.</translation>
    </message>
    <message>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Non è stato trovato il nome host proxy (nome host proxy non valido).</translation>
    </message>
    <message>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>La connessione al proxy è scaduta oppure il proxy non ha risposto in tempo alla richiesta inviata.</translation>
    </message>
    <message>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Per elaborare la richiesta il proxy richiede l&apos;autenticazione ma non ha accettato (se presenti) le credenziali offerte.</translation>
    </message>
    <message>
        <source>An unknown proxy-related error was detected.</source>
        <translation>È stato rilevato un errore sconosciuto relativo al proxy.</translation>
    </message>
    <message>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>L&apos;accesso al contenuto remoto è stato negato (simile all&apos;errore HTTP 403).</translation>
    </message>
    <message>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>L&apos;operazione richiesta sul contenuto remoto non è consentita.</translation>
    </message>
    <message>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Nel server non è stato trovato il contenuto remoto (simile all&apos;errore HTTP 404).</translation>
    </message>
    <message>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Per elaborare la richiesta il server remoto richiede l&apos;autenticazione ma le credenziali fornite (se presenti) non sono state accettate.</translation>
    </message>
    <message>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>La richiesta doveva essere inviata di nuovo, ma questo non è riuscito ad esempio perché non è stato possibile leggere i dati di caricamento una seconda volta.</translation>
    </message>
    <message>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Impossibile completare la richiesta a causa di un conflitto con lo stato attuale della risorsa.</translation>
    </message>
    <message>
        <source>The requested resource is no longer available at the server.</source>
        <translation>La risorsa richiesta non è più disponibile nel server.</translation>
    </message>
    <message>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>È stato rilevato un errore sconosciuto relativo al contenuto remoto.</translation>
    </message>
    <message>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>L&apos;API di accesso alla rete non può soddisfare la richiesta perché il protocollo è sconosciuto.</translation>
    </message>
    <message>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>L&apos;operazione richiesta non è valida per questo protocollo.</translation>
    </message>
    <message>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>È stata rilevata un&apos;interruzione del protocollo (errore di analisi, risposte non valide o impreviste, ecc.).</translation>
    </message>
    <message>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Il server ha riscontrato una condizione imprevista che gli ha impedito di soddisfare la richiesta.</translation>
    </message>
    <message>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Il server non supporta la funzionalità necessaria per soddisfare la richiesta.</translation>
    </message>
    <message>
        <source>The server is unable to handle the request at this time.</source>
        <translation>In questo momento il server non è in grado di gestire la richiesta.</translation>
    </message>
    <message>
        <source>Unknown Server Error.</source>
        <translation>Errore server sconosciuto.</translation>
    </message>
    <message>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Il server remoto ha rifiutato la connessione (il server non accetta richieste).</translation>
    </message>
    <message>
        <source>Unknown Error.</source>
        <translation>Errore sconosciuto.</translation>
    </message>
</context>
</TS>
