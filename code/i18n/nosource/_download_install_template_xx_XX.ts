<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>DownloadInstall</name>
    <message>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Downloading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The download is complete.&lt;br&gt;The current version will be uninstalled before the new one is installed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection to the remote server timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unknown network-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unknown proxy-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The requested resource is no longer available at the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unknown error related to the remote content was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The requested operation is invalid for this protocol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The server is unable to handle the request at this time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown Server Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown Error.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
