//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          DOWNLOADINSTALL
//          Copyright 2022 -2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


#include "download_install.h"
QString DownloadInstall::networkErrorMessages(QNetworkReply::NetworkError error)
{
    QString errorstring;

    switch(error) {
        case QNetworkReply::NoError:
            errorstring = tr("No error.");
            break;

        case QNetworkReply::RemoteHostClosedError:
            errorstring = tr("The remote server closed the connection prematurely, before the entire reply was received and processed.");
            break;

        case QNetworkReply::HostNotFoundError:
            errorstring = tr("The remote host name was not found (invalid hostname).");
            break;

        case QNetworkReply::TimeoutError:
            errorstring = tr("The connection to the remote server timed out.");
            break;

        case QNetworkReply::OperationCanceledError:
            errorstring = tr("The operation was canceled via calls to abort() or close() before it was finished.");
            break;

        case QNetworkReply::SslHandshakeFailedError:
            errorstring = tr("The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.");
            break;

        case QNetworkReply::TemporaryNetworkFailureError:
            errorstring = tr("The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.");
            break;

        case QNetworkReply::NetworkSessionFailedError:
            errorstring = tr("The connection was broken due to disconnection from the network or failure to start the network.");
            break;

        case QNetworkReply::BackgroundRequestNotAllowedError:
            errorstring = tr("The background request is not currently allowed due to platform policy.");
            break;

        case QNetworkReply::TooManyRedirectsError:
            errorstring = tr("While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().");
            break;

        case QNetworkReply::InsecureRedirectError:
            errorstring = tr("While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).");
            break;

        case QNetworkReply::UnknownNetworkError:
            errorstring = tr("An unknown network-related error was detected.");
            break;

        case QNetworkReply::ProxyConnectionRefusedError:
            errorstring = tr("The connection to the proxy server was refused (the proxy server is not accepting requests).");
            break;

        case QNetworkReply::ProxyConnectionClosedError:
            errorstring = tr("The proxy server closed the connection prematurely, before the entire reply was received and processed.");
            break;

        case QNetworkReply::ProxyNotFoundError:
            errorstring = tr("The proxy host name was not found (invalid proxy hostname).");
            break;

        case QNetworkReply::ProxyTimeoutError:
            errorstring = tr("The connection to the proxy timed out or the proxy did not reply in time to the request sent.");
            break;

        case QNetworkReply::ProxyAuthenticationRequiredError:
            errorstring = tr("The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).");
            break;

        case QNetworkReply::UnknownProxyError:
            errorstring = tr("An unknown proxy-related error was detected.");
            break;

        case QNetworkReply::ContentAccessDenied:
            errorstring = tr("The access to the remote content was denied (similar to HTTP error 403).");
            break;

        case QNetworkReply::ContentOperationNotPermittedError:
            errorstring = tr("The operation requested on the remote content is not permitted.");
            break;

        case QNetworkReply::ContentNotFoundError:
            errorstring = tr("The remote content was not found at the server (similar to HTTP error 404).");
            break;

        case QNetworkReply::AuthenticationRequiredError:
            errorstring = tr("The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).");
            break;

        case QNetworkReply::ContentReSendError:
            errorstring = tr("The request needed to be sent again, but this failed for example because the upload data could not be read a second time.");
            break;

        case QNetworkReply::ContentConflictError:
            errorstring = tr("The request could not be completed due to a conflict with the current state of the resource.");
            break;

        case QNetworkReply::ContentGoneError:
            errorstring = tr("The requested resource is no longer available at the server.");
            break;

        case QNetworkReply::UnknownContentError:
            errorstring = tr("An unknown error related to the remote content was detected.");
            break;

        case QNetworkReply::ProtocolUnknownError:
            errorstring = tr("The Network Access API cannot honor the request because the protocol is not known.");
            break;

        case QNetworkReply::ProtocolInvalidOperationError:
            errorstring = tr("The requested operation is invalid for this protocol.");
            break;

        case QNetworkReply::ProtocolFailure:
            errorstring = tr("A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).");
            break;

        case QNetworkReply::InternalServerError:
            errorstring = tr("The server encountered an unexpected condition which prevented it from fulfilling the request.");
            break;

        case QNetworkReply::OperationNotImplementedError:
            errorstring = tr("The server does not support the functionality required to fulfill the request.");
            break;

        case QNetworkReply::ServiceUnavailableError:
            errorstring = tr("The server is unable to handle the request at this time.");
            break;

        case QNetworkReply::UnknownServerError:
            errorstring = tr("Unknown Server Error.");
            break;

        case QNetworkReply::ConnectionRefusedError:
            errorstring = tr("The remote server refused the connection (the server is not accepting requests).");
            break;

        default:
            errorstring = tr("Unknown Error.");
    }

    return errorstring;
}


