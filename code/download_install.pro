#  ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#
#    		DOWNLOADINSTALL
#    		Copyright (C) 2022 Ingemar Ceicer
#    		https://gitlab.com/posktomten/streamcapture2/
#    		ic_streamcapture2@ceicer.com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
# ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((
# 2022-06-04
QT += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
# DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x0602ff // Qt6.2
#ipac
CONFIG+=staticlib

HEADERS += \
    download_install.h \
    download_install_global.h

SOURCES += \
        download_install.cpp \
        networkerrormessages.cpp

FORMS += \
    download_install.ui

#Export LIBS
DEFINES += DOWNLOAD_INSTALL_LIBRARY

TEMPLATE = lib

# By default, qmake will make a shared library. Uncomment to make the library
# static.
# CONFIG += staticlib

#CONFIG += lrelease embed_translations

# By default, TARGET is the same as the directory, so it will make 
# liblibrary.so or liblibrary.a (in linux).  Uncomment to override.

CONFIG(release, debug|release):BUILD=RELEASE
CONFIG(debug, debug|release):BUILD=DEBUG


equals(BUILD,RELEASE) {

    TARGET=download_install

}
equals(BUILD,DEBUG) {

    TARGET=download_installd
}

equals(QT_MAJOR_VERSION, 5) {

DESTDIR=$$OUT_PWD-lib
#DESTDIR=C:\Users\posktomten\PROGRAMMERING\streamcapture2\lib5
#DESTDIR=C:\Users\posktomten\PROGRAMMERING\hashsum\lib5
}

equals(QT_MAJOR_VERSION, 6) {

DESTDIR=$$OUT_PWD-lib
#DESTDIR=C:\Users\posktomten\PROGRAMMERING\streamcapture2\lib6
}

UI_DIR = ../code

TRANSLATIONS += i18n/_download_install_sv_SE.ts \
                i18n/_download_install_it_IT.ts \
                i18n/_download_install_template_xx_XX.ts

# RESOURCES += \
#    resurs_download_install.qrc

#RC_ICONS = images/font.ico

message (--------------------------------------------------)
message (OS: $$QMAKE_HOST.os)
message (Arch: $$QMAKE_HOST.arch)
message (Cpu count: $$QMAKE_HOST.cpu_count)
#message (Name: $$QMAKE_HOST.name)
#message (Version: $$QMAKE_HOST.version)
#message (Version string: $$QMAKE_HOST.version_string)
compiler=$$basename(QMAKESPEC)
message(compiler: $$compiler)
message (qmake path: $$QMAKE_QMAKE)
message (Qt version: $$QT_VERSION)
message(*.pro path: $$_PRO_FILE_PWD_)
message(TEMPLATE: $$TEMPLATE)
message(TARGET: $$TARGET)
message(DESTDIR: $$DESTDIR)
#message(HEADERS $$HEADERS)
#message(SOURCES: $$SOURCES)
#message(INCLUDEPATH $$INCLUDEPATH)
message(LIBS: $$LIBS)
message(PWD: $$PWD)
message (--------------------------------------------------)

