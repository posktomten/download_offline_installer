cmake_minimum_required(VERSION 3.16)
project(download_install VERSION 1.0 LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)

find_package(QT NAMES Qt5 Qt6 REQUIRED COMPONENTS Core Linguist)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Gui Network Linguist)
find_package(Qt${QT_VERSION_MAJOR} OPTIONAL_COMPONENTS Widgets)

SET(CMAKE_AUTOUIC ON)
SET(CMAKE_AUTOMOC ON)
SET(CMAKE_AUTORCC ON)

set(MY_TS_FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/i18n/_download_install_it_IT.ts
    ${CMAKE_CURRENT_SOURCE_DIR}/i18n/_download_install_sv_SE.ts
    ${CMAKE_CURRENT_SOURCE_DIR}/i18n/_download_install_template_xx_XX.ts)



if (WIN32 AND NOT MSVC)
SET(LIBRARY_OUTPUT_PATH  ${CMAKE_CURRENT_BINARY_DIR}../${CMAKE_CURRENT_BINARY_DIR}-lib)
SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}../${CMAKE_CURRENT_BINARY_DIR}-lib)
endif (WIN32 AND NOT MSVC)

if (MSVC)
SET(LIBRARY_OUTPUT_PATH  ${CMAKE_CURRENT_BINARY_DIR}../${CMAKE_CURRENT_BINARY_DIR}-lib)
SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}../${CMAKE_CURRENT_BINARY_DIR}-lib)
endif (MSVC)

if (UNIX)
SET(LIBRARY_OUTPUT_PATH  ${CMAKE_CURRENT_BINARY_DIR}-lib)
SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}-lib)
endif (UNIX)

SET(CMAKE_DEBUG_POSTFIX "d")

#qt_standard_project_setup()



qt_add_library(download_install
    STATIC
    download_install.cpp
    download_install.h
    download_install.ui
    download_install_global.h
    networkerrormessages.cpp
    ${MY_TS_FILES}
)
target_compile_definitions(download_install PUBLIC
    DOWNLOAD_INSTALL_LIBRARY
)

target_link_libraries(download_install PUBLIC

    Qt::Core
    Qt::Gui
    Qt::Network
)

if((QT_VERSION_MAJOR GREATER 4))
    target_link_libraries(download_install PUBLIC
        Qt::Widgets
    )
endif()

install(TARGETS download_install
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    FRAMEWORK DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

qt_add_translations(
    download_install
    TS_FILES ${MY_TS_FILES}
    LUPDATE_OPTIONS -noobsolete
    LUPDATE_OPTIONS -locations none
)

 # qt_add_lupdate(download_install TS_FILES ${TS_FILES})
